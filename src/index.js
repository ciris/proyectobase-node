"use strict";

var express = require( "express" );
var cors = require( "cors" );
var app = express();
var bodyParser = require( "body-parser" );
var Log = require( "log" );
var config = require( "./config.js" );

var log = new Log( "debug" );
var corsOptions = {
  origin: require( "./commons/origin.js" )( config.ORIGIN ),
  credentials: true
};
app.options( "*", cors( corsOptions ) );
app.use( cors( corsOptions ) );
log.debug( "Configuring server" );
app.use( bodyParser.urlencoded( {
  extended: true
} ) );
app.use( bodyParser.json() );

/***************** ROUTER **********************/
var middleware = require( "./login/middleware.js" );
var isAuthorized = middleware.isAuthorized;

var loginGoogle = require( "./login/google.js" );
app.use( "/api/login", loginGoogle );

var loginOdoo = require( "./login/odoo.js" );
app.use( "/api/login", loginOdoo );

var users = require( "./routes/user.js" );
app.use( "/api/user", isAuthorized, users );

var roles = require( "./routes/role.js" );
app.use( "/api/role", isAuthorized, roles );

app.get( "/", function( req, res ) {
  return res.status( 200 ).end( "API is working correctly" );
} );

/**************** Global routes  **********************/

app.use( function( req, res ) {
  res.status( 404 ).send( "Resource not found." );
} );

app.use( function( err, req, res ) {
  console.error( err.stack );
  res.status( 500 ).send( "Server error" );
} );

/****************  Start server *************/

var port = config.PORT;
log.debug( "Loading connections to MongoDB" );
require( "./bd.js" ); //load mongo
app.listen( port );
log.info( "Server running on port", port );
module.exports = app;
