#!/bin/bash
export NVM_DIR="~/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
nvm use 4.0
git pull origin desarrollo
npm install
forever stopall
export ORIGIN=http://ciriscr.com
forever start src
