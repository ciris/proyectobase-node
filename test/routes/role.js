"use strict";

var request = require( "supertest" );
var token = require( "../token.js" );
var role = require( "../../src/models/role.js" ).model;
var _ = require( "lodash" );

describe( "Role module routes", function() {
  this.timeout( 15000 );
  var server;
  beforeEach( function() {
    server = require( "../../src/index.js" );
  } );

  it( "Responds 401 when the request is not authorized", noAuth );
  it( "Responds to get /api/role/", getResponse );
  it( "Responds to post /api/role/ with the created obj", postBase );
  it( "Responds to delete /api/role/:id with the obj marked as deleted", remove );
  it( "Responds to get /api/role/:id with the obj", getOne );
  it( "Responds to put /api/role/:id with the modified obj", update );

  function noAuth( done ) {
    request( server )
      .get( "/api/role" )
      .expect( 401, done );
  }

  function getResponse( done ) {
    request( server )
      .get( "/api/role" )
      .set( "Authorization", token )
      .expect( 200 )
      .expect( checkList )
      .end( finalizeList( done ) );
  }

  function postBase( done ) {
    var temp = new role( {name: "Role object"} );
    request( server )
      .post( "/api/role" )
      .send( temp )
      .set( "Authorization", token )
      .expect( checkPost )
      .end( finalize( done, temp._id.toString() ) );
  }

  function remove( done ) {
    var temp = new role( {name: "Object to remove"} );
    temp.save( function test() {
      request( server )
        .delete( "/api/role/" + temp._id )
        .set( "Authorization", token )
        .expect( checkDelete )
        .end( finalize( done, temp._id.toString() ) );
    } );
  }

  function getOne( done ) {
    var temp = new role( {name: "Object to get"} );
    temp.save( function test() {
      request( server )
        .get( "/api/role/" + temp._id )
        .set( "Authorization", token )
        .expect( 200 )
        .expect( checkGet( temp ) )
        .end( finalize( done, temp._id.toString() ) );
    } );
  }

  function update( done ) {
    var temp = new role( {name: "Object to get"} );
    var newObj = {name: "Other", _id: temp._id};
    temp.save( function test() {
      request( server )
        .put( "/api/role/" + temp._id )
        .send( newObj )
        .set( "Authorization", token )
        .expect( 200 )
        .expect( checkUpdate )
        .end( finalize( done, temp._id.toString() ) );
    } );
  }
} );

function checkList( res ) {
  if ( _.isUndefined( res.body.counter ) || _.isUndefined( res.body.docs ) ) {
    throw new Error( "Incorrect response" );
  }
}

function checkUpdate( res ) {
  if ( res.body.name !== "Other" ) {
    throw new Error( "The name was not updated" );
  }
}

function checkGet( obj ) {
  return function inner( res ) {
    if ( res.body._id.toString() !== obj._id.toString() ) {
      throw new Error( "ID is not the same" );
    }
  };
}

function checkPost( res ) {
  if ( !res.body._id ) {
    throw new Error( "Object does not have ID" );
  }
  if ( res.status !== 200 ) {
    throw new Error( "Response code is not 200" );
  }
}

function checkDelete( res ) {
  if ( res.status !== 200 ) {
    throw new Error( "Response code is not 200" );
  }
}

function finalize( done, id ) {
  return function finalizeInner( err ) {
    role.remove( {"_id": id}, function() {
      if ( err ) {
        return done( err );
      }
      return done();
    } );
  };
}

function finalizeList( done ) {
  return function finalizeInner( err ) {
    if ( err ) {
      return done( err );
    }
    return done();
  };
}
